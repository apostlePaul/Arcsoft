
// ArcFaceDemoDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "ArcFaceEngine.h"

#include <memory>
#include <string>

//调用GDI+
#include <GdiPlus.h>
#include "afxwin.h"
#pragma comment(lib, "Gdiplus.lib")
using namespace Gdiplus;

typedef struct RegFrInfo {
	ASF_FaceFeature fr; //人脸特征
	CString strName;    //姓名
};

typedef struct CmpFrInfo {
	ASF_FaceFeature fr; //人脸特征
	Gdiplus::Rect faceRect; //人脸框
	CString strName;        //姓名
	float fThreshold;   //比对阈值
};

// CArcFaceDemoDlg 对话框
class CArcFaceDemoDlg : public CDialogEx
{
	// 构造
public:
	CArcFaceDemoDlg(CWnd* pParent = NULL);	// 标准构造函数
	~CArcFaceDemoDlg();
	// 对话框数据
	enum { IDD = IDD_ARCFACEDEMO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


	// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedBtnRegister();
	afx_msg void OnBnClickedBtnRecognition();
	afx_msg void OnBnClickedBtnCompare();
	afx_msg void OnBnClickedBtnClear();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedBtnCamera();
	afx_msg void OnEnChangeEditThreshold();

	void EditOut(CString str, bool add_endl=TRUE);
	void IplDrawToHDC(BOOL isVideoMode, IplImage* rgbImage, CRect& showRect, UINT ID);//画人脸框

private:
	void start_compare(CmpFrInfo& cmpFrInfo);
	void LoadThumbnailImages();
	CString SelectFolder();
	BOOL TerminateLoadThread();
	BOOL ClearRegisterImages();
	BOOL CalculateShowPositon(IplImage*curSelectImage, Gdiplus::Rect& showRect);
	MRESULT StaticImageFaceOp(IplImage* image);
	void ClearShowWindow();

public:
	CListCtrl m_ImageListCtrl;
	CImageList m_IconImageList;
	CEdit m_editLog;

	CString m_folderPath;
	CString m_strEditThreshold;			   //比对的阈值
	IplImage* m_curStaticImage;			   //当前比对的照片
	Gdiplus::Rect m_curImageShowRect;      //当前比对照片的区域位置
	BOOL m_curStaticImageFRSucceed;		   //当前比对照片特征提取标志
	std::vector<CmpFrInfo> m_cmpFrInfoVec; //当前比对人脸特征信息容器
	std::vector<RegFrInfo> m_regFrInfoVec; //注册人脸特征信息容器
	
	BOOL m_bLoadIconThreadRunning;
	DWORD m_dwLoadIconThreadID;
	HANDLE m_hLoadIconThread;

	BOOL m_bClearFeatureThreadRunning;
	DWORD m_dwClearFeatureThreadID;
	HANDLE m_hClearFeatureThread;

	BOOL m_bFDThreadRunning;
	DWORD m_dwFDThreadID;
	HANDLE m_hFDThread;

	BOOL m_bFRThreadRunning;
	DWORD m_dwFRThreadID;
	HANDLE m_hFRThread;

	ArcFaceEngine m_imageFaceEngine;
	ArcFaceEngine m_videoFaceEngine;
	
	CString m_curStaticShowAgeGenderString;
	
	IplImage* m_curVideoImage;
	IplImage* m_curIrVideoImage;
	ASF_SingleFaceInfo m_curFaceInfo;
	bool m_dataValid;
	bool m_irDataValid;

	bool m_videoOpened;


	Gdiplus::PointF m_curStringShowPosition;	//当前字符串显示的位置
	CString m_curVideoShowString;
	CString m_curIRVideoShowString;
	CFont* m_Font;
private:
	CRect m_windowViewRect;						//展示控件的尺寸

public:
	afx_msg void OnClose();
	afx_msg void OnLvnItemchangedListImage(NMHDR *pNMHDR, LRESULT *pResult);
};
